#!/bin/sh
# This script is used to create a bombic.app for OSX.
# it is not needed under Linux at all.

rm -rf Bombic.app
mkdir Bombic.app

cp -Rf template.app/*  Bombic.app

# copy frameworks

cp -R /Library/Frameworks/SDL.framework       Bombic.app/Contents/Frameworks
cp -R /Library/Frameworks/SDL_image.framework Bombic.app/Contents/Frameworks
cp -R /Library/Frameworks/SDL_mixer.framework Bombic.app/Contents/Frameworks
cp -R /Library/Frameworks/SDL_ttf.framework   Bombic.app/Contents/Frameworks


{
cd Bombic.app/Contents/Frameworks
tar -zxf ImageIO.framework.tar.gz
rm ImageIO.framework.tar.gz
cd ../../..
}

# copy data

{
cp -Rf data/*  Bombic.app/Contents/Resources
}


# remove unnecessary stuff
{
cd Bombic.app 
find . -name '.svn' -exec rm -rf {} \;
find . -name 'Makefile*' -exec rm -rf {} \;
find . -name 'create.am.sh' -exec rm -rf {} \;
find . -name '.DS_Store' -exec rm -rf {} \;
}


