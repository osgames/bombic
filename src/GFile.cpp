
#include "stdafx.h"
#include "D3DXApp.h"
#include "GFile.h"
#include "data.h"
#include "searchfile.h"

#include "tinyxml.h"
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

using namespace std;

// This is some initial minimal setting - it is rewriten by datafiles.

st_zed P_zed[MAX_ZED] = {
	{"zem",                  0, 0, true,  0, 1, 0, 1, false},
	{"strana leva",          1, 0, false, 0, 1, 0, 1, false},
	{"strana leva-horni",    2, 0, false, 0, 1, 0, 1, false},
	{"strana horni",         3, 0, false, 0, 1, 0, 1, false},
	{"strana prava-horni",   4, 0, false, 0, 1, 0, 1, false},
	{"strana prava",         5, 0, false, 0, 1, 0, 1, false},
	{"strana leva-doli",     6, 0, false, 0, 1, 0, 1, false},
	{"strana dolni",         7, 0, false, 0, 1, 0, 1, false},
	{"strana prava dolni",   8, 0, false, 0, 1, 0, 1, false},
	{"nepruchodna",          9, 0, false, 0, 1, 0, 1, false},
	{"nepruchodna s teckou", 0, 1, false, 3, 3, 0, 1, false},
	{"znicitelna",           4, 1, false, 0, 1, 5, 3, true},
};

st_mrcha P_mrcha[MAX_MRCH] = {
	{"Koule",          4, 4, 2, 1, 100, 50, 70, false, false, 255, 30},
	{"Slizoun",         2, 4, 3, 2, 100, 50, 70, false, false, 255, 30},
	{"Mracek",          4, 4, 2, 1, 100, 50, 70, false, false, 255, 30},
	{"Mesicek",         2, 4, 3, 2, 100, 50, 70, false, false, 255, 30},
};

st_bomber P_bomber = {4, 8, 2, 50, 70, 30};

#define DAT_FILE "bomber.dat"

#define DAT_FILE_BOMBER "bomber.xml"
#define DAT_FILE_MONSTR "monstr.xml"
#define DAT_FILE_WALLS  "walls.xml"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

GFile::GFile()
{

}

GFile::~GFile()
{

}


// Part for saving data
template<class T> T StoT(const char *s)
{
	T tmp;
	stringstream st(s);
	st >> tmp;
	return tmp;
}

#define OPEN_FILE(file, main_tag, element_tag) \
	filename = locate_file(file); \
	sz = 0; \
	if (!doc.LoadFile(filename)) \
	{ \
		cerr << "Unable to load " << filename \
		     << " Probably file not exist or isn't a valid XML file" << endl; \
		return false; \
	} \
	child = handle.FirstChild( main_tag ).FirstChild( element_tag ).Element();

#define GET_VALUE(x) \
	value = child->Attribute( #x ); \
	if ( value == 0 ) \
	{ \
		cerr << "Missing attribute " << #x << " in file " << filename << endl; \
		return false; \
	} 

#define LOAD(x) \
	GET_VALUE(x) \
	{ \
		stringstream st(value); \
		st >> tmp.x; \
	}
		
#define LOAD_CHARS(x) \
	GET_VALUE(x) \
	strncpy(tmp.x, value, 29);
	
#define LOAD_ID \
	GET_VALUE(id) \
	int ID; \
	{ \
		stringstream st(value); \
		st >> ID; \
	}



bool GFile::LoadData()
{
/*
	const char *df = locate_file(DAT_FILE);
	if (df == NULL) return false;
	fstream f(df, ios::in|ios::binary);
	if (!f) {
		cerr << "Unalbe to read file " << DAT_FILE << endl;
		return false;
	}
	f.read((char*)&P_bomber, sizeof(st_bomber));
	f.read((char*)&P_mrcha[0], sizeof(st_mrcha) * MAX_MRCH);
	f.read((char*)&P_zed[0], sizeof(st_zed) * MAX_ZED);	
	f.close();

	SaveData();
*/
		
	string filename;
		
	TiXmlDocument doc;
	TiXmlHandle   handle(&doc);
	TiXmlElement* child;
	int sz;
	const char *value;


	// ----- BOMBIC -----
	OPEN_FILE(DAT_FILE_BOMBER, "bombers", "bomber")
	{
		st_bomber tmp;
		
		LOAD(speed)
		LOAD(anims)
		LOAD(animspeed)
		LOAD(b_x)
		LOAD(b_y)
		LOAD(dy)
	
		P_bomber = tmp;		
	}
	

	// ----- MONSTERS -----
	OPEN_FILE(DAT_FILE_MONSTR, "monsters", "monster")

	for (; child; child=child->NextSiblingElement()) 
	{
		st_mrcha tmp;
		
		LOAD_CHARS(name)
		LOAD(speed)
		LOAD(anims)
		LOAD(animspeed)
		LOAD(lives)
		LOAD(score)
		LOAD(b_x)
		LOAD(b_y)
		LOAD(fly)
		LOAD(sizing)
		LOAD(alpha)
		LOAD(dy)
		LOAD(danims)
		LOAD(ai)
		
		LOAD_ID
		P_mrcha[ID] = tmp;		
	}


	// ----- WALLS -----
	OPEN_FILE(DAT_FILE_WALLS, "walls", "wall")

	for (; child; child=child->NextSiblingElement()) 
	{
		st_zed tmp;
		
		LOAD_CHARS(name)
		LOAD(sx)
		LOAD(sy)
		LOAD(walk)
		LOAD(anims)
		LOAD(anim_speed)
		LOAD(animbum)
		LOAD(animsbum_speed)
		LOAD(destructable)
		LOAD(firein)
		
		LOAD_ID
		P_zed[ID] = tmp;		
	}

	return true;
}


// Part for saving data
template<class T> string TtoS(T &var)
{
	stringstream st;
	st << var;
	return st.str();
}

#define SAVE(x) \
	variable.SetAttribute(#x, TtoS(tmp->x));
		


#define INSERT_VAL_AND_SAVE_DOC \
	if (doc.InsertEndChild(values) == NULL) { \
		cerr << "Data::save_values InsertEndChild failed.\n"; \
		return; \
	} \
	if (!doc.SaveFile()) { \
		cerr << "Data::save_values Unable to save file.\n"; \
		return; \
	} \


void GFile::SaveData()
{
/*	
	fstream f(DAT_FILE, ios::out|ios::binary);
	if (!f) {
		cerr << "Unalbe to write file" << DAT_FILE << endl;
		return;
	}
	f.write((const char*)&P_bomber, sizeof(st_bomber));
	f.write((const char*)&P_mrcha[0], sizeof(st_mrcha) * MAX_MRCH);
	f.write((const char*)&P_zed[0], sizeof(st_zed) * MAX_ZED);
	f.close();
*/
	
	{
		TiXmlDocument doc(DAT_FILE_BOMBER);
		TiXmlElement  values("bombers"); // root element
		
		TiXmlElement variable("bomber");
		
		st_bomber* tmp = & P_bomber;

		SAVE(speed);
		SAVE(anims);
		SAVE(animspeed);
		SAVE(b_x);
		SAVE(b_y);
		SAVE(dy);

		values.InsertEndChild(variable);
	
		INSERT_VAL_AND_SAVE_DOC

	}
	{	
		TiXmlDocument doc(DAT_FILE_MONSTR);
		TiXmlElement  values("monsters"); // root element
		
		for (int i = 0; i < MAX_MRCH; ++i)
		{
			TiXmlElement variable("monster");
			st_mrcha *tmp = &P_mrcha[i];
			variable.SetAttribute("id", TtoS(i));
	
			SAVE(name);
			SAVE(speed);
			SAVE(anims);
			SAVE(animspeed);
			SAVE(lives);
			SAVE(score);
			SAVE(b_x);
			SAVE(b_y);
			SAVE(fly);
			SAVE(sizing);
			SAVE(alpha);
			SAVE(dy);
			SAVE(danims);
			SAVE(ai);
			
		 	values.InsertEndChild(variable);
		}
	
		INSERT_VAL_AND_SAVE_DOC	
	}
	{	
		TiXmlDocument doc(DAT_FILE_WALLS);
		TiXmlElement  values("walls"); // root element
		
		for (int i = 0; i < MAX_ZED; ++i)
		{
			TiXmlElement variable("wall");
			st_zed *tmp = &P_zed[i];
			variable.SetAttribute("id", TtoS(i));
	
			SAVE(name);
			SAVE(sx);
			SAVE(sy);
			SAVE(walk);
			SAVE(anims);
			SAVE(anim_speed);
			SAVE(animbum);
			SAVE(animsbum_speed);
			SAVE(destructable);
			SAVE(firein);
			
		 	values.InsertEndChild(variable);
		}
	
		INSERT_VAL_AND_SAVE_DOC	
	}
}


//headers
int xinu(int _unix);
void xinu_p(void* _unix);
short ba(short ab);
void xinu_mem(void *m, size_t sz);
void ba_mem(void *m, size_t sz);

bool g_nuxi = false; ///< Do we need nuxi?


#define NUXI_FILE "nuxi"



bool nuxi()
{
	int i;
	
	fstream f(locate_file(NUXI_FILE), ios::in|ios::binary);
	if (!f) {
		cerr <<	"Unable to decide nuxi" << endl;
		return false;
	}
	
	f.read((char*)&i, sizeof(int));
	f.close();
	
	if (  i == (((int)'u' << 24) | ((int)'n' << 16) | ((int)'i' << 8) | 'x') ) 
	{
//		cerr << "HURA" << endl;
		g_nuxi = true;
		return true;
	}
	else
	{
		g_nuxi = false;
		return false;
	}
}

void nuxi_map(st_map *map)
{
	if (nuxi())
	{
	//	cout << "Nuxing map " <<  map->podklad << endl;
	
		xinu_p(&map->podklad);
		xinu_p(&map->x);
		xinu_p(&map->y);
		for (int i = 0; i < MAX_X; i++)
			for (int j = 0; j < MAX_Y; j++)
			{
				xinu_p(&map->map[i][j]);
				xinu_p(&map->mapmrch[i][j]);
			}
	//	cout << "Map nuxed " << map->podklad << endl;
	}
	
}

void GFile::SaveMap(st_map *map, const string name)
{

	fstream f(name.c_str(), ios::out|ios::binary);
	if (!f) {
		cerr <<	"Unable to save map" << name << endl;
	}
	
//	nuxi_map(map);
	// allways save in little-endian
	f.write((const char*)map, sizeof(st_map));
//	nuxi_map(map);

	f.close();
}

void GFile::LoadMap(st_map *map, const string file)
{
	const char *df = locate_file(file.c_str());
	if (df == NULL) return;
	fstream f(df, ios::in|ios::binary);
	if (!f) {
		cerr << "Unable to open file" << file << endl;
	}
	
	// allways load in little-endian
	f.read((char*)map, sizeof(st_map));
	nuxi_map(map);

	f.close();


}


/**
 * \brief Transform bytes in int to undo nuxi problem
 *
 * xinu
 */
int xinu(int _unix)
{
	int i = 0;

//	cout << _unix << " -> ";

	i |= ((_unix >> 24) & 0xFF) <<  0; // u
	i |= ((_unix >> 16) & 0xFF) <<  8; // n
	i |= ((_unix >>  8) & 0xFF) << 16; // i
	i |= ((_unix >>  0) & 0xFF) << 24; // x

//	cout << i << /*" " << *((float*)((void*)&i)) << */  endl;

	return i;
}

void xinu_p(void* _unix)
{
	if (!g_nuxi) return;
	
	*(int*)_unix = xinu(*(int*)_unix);
}

short ba(short ab)
{
	return (((ab & 0xFF) << 8) | ((ab >> 8)& 0xFF));
}

/**
 * \brief Nuxe a memory full of ints
 */
void xinu_mem(void *m, size_t sz)
{
	if (!g_nuxi) return;

	int *pi = (int*)m;

	for (; sz > 0; sz -= 4, ++pi)
	{
		*pi = xinu(*pi);
	}
}


/**
 * \brief ba a memory
 */
void ba_mem(void *m, size_t sz)
{
	if (!g_nuxi) return;

	short *ps = (short*)m;

	for (; sz > 0; sz -= 2, ++ps)
	{
		*ps = ba(*ps);
	}
}
