#ifndef _GFILE_H_
#define _GFILE_H_

#include <string>
#include "data.h"


/* This class is for loading and saving datafiles - monsters config and maps.
 * It needs rewrite to work vith text data and not with binary data.
 * [then the game may run on arch != i386]
 * */

class GFile  
{
public:
	static void LoadMap(st_map *map, const std::string file);
	static void SaveMap(st_map *map, const std::string name);
	static void SaveData();
	static bool LoadData();
	GFile();
	virtual ~GFile();

};

#endif
