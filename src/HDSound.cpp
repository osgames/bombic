/*  Hippo Games - 2005 (C) - http://hippo.nipax.cz
 *    __________________________________________________
 *   /\     __  __    _____   ______   ______   ______  \
 *   \ \   /\ \|\ \  /\_  _\ /\  __ \ /\  __ \ /\  __ \  \
 *    \ \  \ \ \_\ \ \//\ \/ \ \ \_\ \\ \ \_\ \\ \ \ \ \  \
 *     \ \  \ \  __ \  \ \ \  \ \  __/ \ \  __/ \ \ \ \ \  \
 *      \ \  \ \ \/\ \  \_\ \_ \ \ \/   \ \ \/   \ \ \_\ \  \
 *       \ \  \ \_\ \_\ /\____\ \ \_\    \ \_\    \ \_____\  \
 *        \ \  \/_/\/_/ \/____/  \/_/     \/_/     \/_____/   \
 *         \ \_________________________________________________\
 *          \/_________________________________________________/
 *                           
 *  
 *  Sub     : Funkce pro Zvuk
 *
 *  File    : HDSound.cpp
 *  
 *  Projekt : 
 *  
 *  Autor   : Bernard Lidicky [bernard@matfyz.cz]
 *
 *  Datum   : 29.12.2000
 *
 */      

#include "stdafx.h"
#include "HDSound.h"
#include <iostream>
#include "searchfile.h"


using namespace std;

HDSound dsound; // sound singleton
extern bool gsoundavailable; // from global setting


HDSound::HDSound()
{
}


HDSound::~HDSound()
{
#ifdef HAVE_SDL_SDL_MIXER_H
	// This should be probably somevere else - before SDL_Quit
	Mix_CloseAudio();
#endif	
}

bool HDSound::Create()
{
#ifdef HAVE_SDL_SDL_MIXER_H
	// SDL audio inicialization
	if(SDL_WasInit(SDL_INIT_AUDIO)==0)
		if (SDL_InitSubSystem(SDL_INIT_AUDIO) < 0 ) {
			cerr << "SDL audio inicizlization failed: " 
				<<  SDL_GetError() << endl;
			return false;
		}
	
	// SDL_Mixer inicialization
// open 44.1KHz, signed 16bit, system byte order,
//      stereo audio, using 1024 byte chunks
	if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 2048)==-1) {
		cerr << "Mix_OpenAudio: " <<  Mix_GetError() << endl;
		return false;
	}
	
	Mix_AllocateChannels(16);
#endif	
	return true;
	
}




HDSoundBuffer::HDSoundBuffer()
{
#ifdef HAVE_SDL_SDL_MIXER_H
	m_sample = NULL;
	m_sample_channel = -1;
#endif		
}

HDSoundBuffer::~HDSoundBuffer()
{
#ifdef HAVE_SDL_SDL_MIXER_H
	if (m_sample != NULL) {
		Mix_FreeChunk(m_sample);
		m_sample = NULL;
	}
#endif		
}

void HDSoundBuffer::Play(bool loop)
{
#ifdef HAVE_SDL_SDL_MIXER_H
	if (!gsoundavailable || !m_sample) return;
	
	if (loop) {
		m_sample_channel = Mix_PlayChannel(-1, m_sample, -1);
	}
	else {
		m_sample_channel = Mix_PlayChannel(-1, m_sample, 0);
	}
#endif		
}

bool HDSoundBuffer::LoadFromRes(const char *resname)
{
#ifdef HAVE_SDL_SDL_MIXER_H

	const char *c = locate_file(resname);
	if (c == NULL) return false;
	m_sample = Mix_LoadWAV(c);
	m_sample_channel = -1;
#endif		
	return true;	
}

void HDSoundBuffer::SetPan(int pan)
{
}

void HDSoundBuffer::SetFreq(int freq)
{

}

void HDSoundBuffer::SetVolume(int volume)
{

}

void HDSoundBuffer::Stop()
{
#ifdef HAVE_SDL_SDL_MIXER_H
	if (m_sample_channel != -1) 
		Mix_HaltChannel(m_sample_channel);
#endif		
}

