/*  Hippo Games - 2000 (C) - http://www.hippo.nipax.cz/
 *    __________________________________________________
 *   /\     __  __    _____   ______   ______   ______  \
 *   \ \   /\ \|\ \  /\_  _\ /\  __ \ /\  __ \ /\  __ \  \
 *    \ \  \ \ \_\ \ \//\ \/ \ \ \_\ \\ \ \_\ \\ \ \ \ \  \
 *     \ \  \ \  __ \  \ \ \  \ \  __/ \ \  __/ \ \ \ \ \  \
 *      \ \  \ \ \/\ \  \_\ \_ \ \ \/   \ \ \/   \ \ \_\ \  \
 *       \ \  \ \_\ \_\ /\____\ \ \_\    \ \_\    \ \_____\  \
 *        \ \  \/_/\/_/ \/____/  \/_/     \/_/     \/_____/   \
 *         \ \_________________________________________________\
 *          \/_________________________________________________/
 *                           
 *  
 *  Sub     : Class for wrapping sound
 *
 *  File    : HDSound.h
 *  
 *  Projekt : HDXII
 *  
 *  Autor   : Bernard Lidicky [bernard@matfyz.cz]
 *
 *  Datum   : 22.5.2005
 *
 */      

#ifndef HDSOUND_HEADER
#define HDSOUND_HEADER

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_SDL_SDL_MIXER_H
#include <SDL_mixer.h>
#endif

class HDSound  
{
public:
	bool Create();

	HDSound();
	virtual ~HDSound();

private:
};


extern HDSound dsound; // one singleton

/**
 * Class for one sound buffer
 */
class HDSoundBuffer 
{
public:
	void Stop();
	void SetVolume(int volume);  // not implemented
	void SetFreq(int freq);      // not implemented
	void SetPan(int pan);        // not implemented
	bool LoadFromRes(const char* resname);
	HDSoundBuffer();
	~HDSoundBuffer();

	void Play(bool loop = false);

private:
	
#ifdef HAVE_SDL_SDL_MIXER_H
	Mix_Chunk *m_sample;
#endif
	
	int 	   m_sample_channel;
};

#endif

