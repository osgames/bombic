// MCredits.cpp: implementation of the MCredits class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MCredits.h"
#include <SDL.h>
#include "config.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MCredits::MCredits()
{

}

MCredits::~MCredits()
{

}

void MCredits::Destroy()
{
	if (m_bPozadi != -1) d3dx.DestroyTexture(m_bPozadi);
	m_bPozadi = -1;
}

void MCredits::Draw()
{
	d3dx.Draw(m_bPozadi, 400, 300, 0, 1.5625);
	
//cerne
	m_fontL->DrawText( 131, 221,  "Bernard Lidický", 0, 0, 0);
//	m_font->DrawText(  211, 217,  "LIDICKÝ", 0, 0, 0);
//	m_font->DrawText(  211, 217,  "LIDICKY", 0, 0, 0);
	m_fontL->DrawText( 131, 246,  "bernard@matfyz.cz", 0,0,0);

//	m_fontL->DrawText( 511, 341,  "Zdeněk", 0,0,0);
//	m_fontL->DrawText(  586, 337,  "BÖSWART", 0,0,0);

	m_fontL->DrawText( 511, 341,  "Zdeněk Böswart", 0,0,0);
//	m_fontL->DrawText(  586, 337,  "BOSWART", 0,0,0);
	m_fontL->DrawText( 511, 366,  "2zdeny@seznam.cz", 0,0,0);

// bileö
	m_fontL->DrawText( 130, 220,  "Bernard Lidický", 255, 255, 255);
//	m_fontL->DrawText(  210, 216,  "LIDICKY", 255, 255, 255);
	m_fontL->DrawText( 130, 245,  "bernard@matfyz.cz", 255,255,255);

	m_fontL->DrawText( 510, 340,  "Zdeněk Böswart", 255,255,255);
//	m_fontL->DrawText(  585, 336,  "BOSWART", 255,255,255);
	m_fontL->DrawText( 510, 365,  "2zdeny@seznam.cz", 255,255,255);

#ifdef VERSION

	m_font->DrawText(  400, 500,  VERSION, 255,255,255);

#endif

}

void MCredits::Init(CMainFrame *parent)
{
	GBase::Init(parent);

	m_bPozadi = d3dx.CreateTextureFromFile("mcredits.jpg");
}

int MCredits::OnKey(int nChar)
{
	switch (nChar) {
	case 27 : 
	case SDLK_RETURN : 
	case SDLK_SPACE  :
		return MENU_MAIN; 
		break;
	}

	return MENU_DEFAULT;
}
