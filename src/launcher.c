/* $Id: launcher.c,v 1.3 2005/04/25 13:07:06 michal-marek Exp $ */

#ifndef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Linux-specific feature
const char * get_exe_dir(void)
{
	static char buf[PATH_MAX];
	int ret;

	ret = readlink("/proc/self/exe", buf, sizeof(buf));
	if (ret <= 0) {
		return 0;
	}
	buf[ret] = 0;
	*strrchr(buf, '/') = 0;
	return buf;
}

int main(int argc, char **argv)
{
	const char *dir = get_exe_dir();
	char libpath[PATH_MAX], bombic[PATH_MAX];

	if (getenv("LD_LIBRARY_PATH")) {
		snprintf(libpath, sizeof(libpath), "LD_LIBRARY_PATH=%s:%s",
				dir, getenv("LD_LIBRARY_PATH"));
	} else {
		snprintf(libpath, sizeof(libpath), "LD_LIBRARY_PATH=%s",
				dir);
	}
	putenv(libpath);
	snprintf(bombic, sizeof(bombic), "%s/bombic", dir);
	execv(bombic, argv);
	perror(bombic);

	return 1;
}

